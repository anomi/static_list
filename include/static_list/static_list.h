#ifndef _STATIC_LIST_H_
#define _STATIC_LIST_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "static_memory_pool/static_memory_pool.h"

/**
 * @brief 
 * 
 */
typedef struct node_t {
    struct node_t* head;    /**< */
    struct node_t* tail;    /**< */
    void* elem;             /**< */
} node_t;

/**
 * @brief 
 * 
 */
typedef struct {
    smpool_t* elem_pool;    /**< */
    smpool_t* node_pool;    /**< */
    node_t* root_node;      /**< */
    size_t elem_size;       /**< */
} slist_header_t;

/**
 * @brief 
 * 
 * @param left
 * @param right
 * @return true
 */
typedef bool (*slist_comparator_t)(void* left, void* right);

/**
 * @brief 
 * 
 * @param capacity
 * @param elem_size
 */
#define slist_reserve(capacity, elem_size) (smpool_reserve(capacity, sizeof(node_t)) + smpool_reserve(capacity, elem_size) + sizeof(slist_header_t))

/**
 * @brief 
 * 
 */
typedef uint8_t slist_t;

/**
 * @brief 
 * 
 * @param l 
 * @param capacity 
 * @param elem_size 
 * @return true 
 * 
 * @sa slist_reserve
 * @note TODO
 */
bool slist_init(slist_t* l, size_t capacity, size_t elem_size);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_push_front(slist_t* l, void* e);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_push_back(slist_t* l, void* e);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_pop_front(slist_t* l, void* e);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_pop_back(slist_t* l, void* e);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_front(slist_t* l, void* e);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_back(slist_t* l, void* e);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_peek_front(slist_t* l, void** e);

/**
 * @brief 
 * 
 * @param l 
 * @param e 
 * @return true  
 */
bool slist_peek_back(slist_t* l, void** e);

/**
 * @brief 
 * 
 * @param l 
 * @return true  
 */
bool slist_clear(slist_t* l);

/**
 * @brief 
 * 
 * @param l 
 * @param empty 
 * @return true  
 */
bool slist_empty(slist_t* l, bool* empty);

/**
 * @brief 
 * 
 * @param l 
 * @param size 
 * @return true  
 */
bool slist_size(slist_t* l, size_t* size);

/**
 * @brief 
 * 
 * @param l 
 * @param capacity 
 * @return true  
 */
bool slist_capacity(slist_t* l, size_t* capacity);

/**
 * @brief 
 * 
 * @param l 
 * @param cmp 
 * @param right 
 * @param e 
 * @return true  
 */
bool slist_find(slist_t* l, slist_comparator_t cmp, void* right, void** e);

// TODO maybe later
// bool slist_sort(slist_t* l, slist_comparator_t cmp, void* right);

#ifdef __cplusplus
}
#endif

#endif // _STATIC_LIST_H_