#include <string.h>
#include "static_list/static_list.h"

bool slist_init(slist_t* l, size_t capacity, size_t elem_size) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    smpool_t* np = l + sizeof(slist_header_t);
    smpool_t* ep = l + sizeof(slist_header_t) + smpool_reserve(capacity, sizeof(node_t));
    if (l != NULL
        && elem_size > 0)
    {
        if (smpool_init(np, capacity, sizeof(node_t)) == true
            && smpool_init(ep, capacity, elem_size) == true)
        {
            h->elem_size = elem_size;
            h->node_pool = np;
            h->elem_pool = ep;

            h->root_node = NULL;
            
            b = true;
        }
    }
    return b;
}

bool slist_push_front(slist_t* l, void* e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && e != NULL)
    {
        node_t* free_node = NULL;
        node_t* new_node = NULL;
        
        if (smpool_aquire(h->node_pool, (void**)&new_node) == true
            && smpool_aquire(h->elem_pool, &new_node->elem) == true)
        {
            new_node->head = NULL;
            new_node->tail = NULL;

            // find next node with a free head
            for(free_node = h->root_node;
                free_node != NULL 
                && free_node->head != NULL;
                free_node = free_node->head);

            if (memcpy(new_node->elem, e, h->elem_size) != NULL)
            {
                if (h->root_node == NULL)
                {
                    h->root_node = new_node;
                    new_node->head = NULL;
                    new_node->tail = NULL;
                }
                else
                {
                    free_node->head = new_node;
                    new_node->tail = free_node;
                    new_node->head = NULL;
                }

                b = true;
            }
            else 
            {
                /* should never happen */
                (void)smpool_release(h->elem_pool, (void*)new_node->elem);
                (void)smpool_release(h->node_pool, (void*)new_node);
            }
        }
    }
    return b;
}

bool slist_push_back(slist_t* l, void* e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && e != NULL)
    {
        node_t* free_node = NULL;
        node_t* new_node = NULL;
        size_t node_id = 0;
        
        if (smpool_aquire(h->node_pool, (void**)&new_node) == true
            && smpool_aquire(h->elem_pool, &new_node->elem) == true)
        {
            new_node->head = NULL;
            new_node->tail = NULL;

            // find next node with a free tail
            for(free_node = h->root_node;
                free_node != NULL 
                && free_node->tail != NULL;
                free_node = free_node->tail);

            if (memcpy(new_node->elem, e, h->elem_size) != NULL)
            {
                if (h->root_node == NULL)
                {
                    h->root_node = new_node;
                    new_node->head = NULL;
                    new_node->tail = NULL;
                }
                else
                {
                    free_node->tail = new_node;
                    new_node->tail = NULL;
                    new_node->head = free_node;
                }
                b = true;
            }
            else 
            {
                /* should never happen */
                (void)smpool_release(h->elem_pool, (void*)new_node->elem);
                (void)smpool_release(h->node_pool, (void*)new_node);
            }
        }
    }
    return b;
}

bool slist_pop_front(slist_t* l, void* e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL
        && e != NULL)
    {
        node_t* head_node = NULL;
        node_t* final_node = NULL;

        // find head node
        for(head_node = h->root_node; head_node->head != NULL; head_node = head_node->head);

        if (memcpy(e, head_node->elem, h->elem_size) != NULL)
        {
            if (head_node->tail != NULL)
            {
                head_node->tail->head = NULL; 
            }
            else
            {
                final_node = head_node;
            }
            
            head_node->head = NULL;
            head_node->tail = NULL;

            if (smpool_release(h->elem_pool, (void*)head_node->elem) == true
                && smpool_release(h->node_pool, (void*)head_node) == true)
            {
                if (final_node != NULL)
                {
                    h->root_node = NULL;
                }
                b = true;
            }
            else
            {
                /* we are in trouble! */
            }
        }
    }
    return b;
}

bool slist_pop_back(slist_t* l, void* e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL
        && e != NULL)
    {
        node_t* tail_node = NULL;
        node_t* final_node = NULL;

        // find tail of slist
        for(tail_node = h->root_node; tail_node->tail != NULL; tail_node = tail_node->tail);

        if (memcpy(e, tail_node->elem, h->elem_size) != NULL)
        {
            if (tail_node->head != NULL)
            {
                tail_node->head->tail = NULL; 
            }
            else
            {
                final_node = tail_node;
            }
            
            tail_node->head = NULL;
            tail_node->tail = NULL;

            if (smpool_release(h->elem_pool, (void*)tail_node->elem) == true
                && smpool_release(h->node_pool, (void*)tail_node) == true)
            {
                if (final_node != NULL)
                {
                    h->root_node = NULL;
                }
                b = true;
            }
            else
            {
                /* we are in trouble! */
            }
        }
    }
    return b;
}

bool slist_front(slist_t* l, void* e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL
        && e != NULL)
    {
        node_t* head_node = NULL;

        // find head node
        for(head_node = h->root_node; head_node->head != NULL; head_node = head_node->head);
        
        b = memcpy(e, head_node->elem, h->elem_size) != NULL ? true : false;
    }
    return b;
}

bool slist_back(slist_t* l, void* e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL
        && e != NULL)
    {
        node_t* tail_node = NULL;

        // find tail of slist
        for(tail_node = h->root_node; tail_node->tail != NULL; tail_node = tail_node->tail);

        b = memcpy(e, tail_node->elem, h->elem_size) != NULL ? true : false;
    }
    return b;
}

bool slist_peek_front(slist_t* l, void** e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL
        && e != NULL)
    {
        node_t* head_node = NULL;

        // find head node
        for(head_node = h->root_node; head_node->head != NULL; head_node = head_node->head);
        *e = head_node->elem;
        b = true;
    }
    return b;
}

bool slist_peek_back(slist_t* l, void** e) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL
        && e != NULL)
    {
        node_t* tail_node = NULL;

        // find tail of slist
        for(tail_node = h->root_node; tail_node->tail != NULL; tail_node = tail_node->tail);
        *e = tail_node->elem;
        b = true;
    }
    return b;
}

bool slist_clear(slist_t* l) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL)
    {
        for (node_t* head_node = h->root_node; head_node->head != NULL; head_node = head_node->head)
        {
            if (smpool_release(h->elem_pool, (void*)head_node->elem) == false
                && smpool_release(h->node_pool, (void*)head_node) == false)
            {
                /* should never happen */
            }
        }

        for (node_t* tail_node = h->root_node; tail_node->tail != NULL; tail_node = tail_node->tail)
        {
            if (smpool_release(h->elem_pool, (void*)tail_node->elem) == false
                && smpool_release(h->node_pool, (void*)tail_node) == false)
            {
                /* should never happen */
            }
        }

        if (smpool_release(h->elem_pool, (void*)h->root_node->elem) == true
            && smpool_release(h->node_pool, (void*)h->root_node) == true)
        {
            h->root_node = NULL;
        }
        else
        {
            /* should never happen */
        }

        b = true;
    }
    return b;
}

bool slist_empty(slist_t* l, bool* empty) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && empty != NULL)
    {
        *empty = h->root_node == NULL ? true : false;
        b = true;
    }
    return b;
}

bool slist_size(slist_t* l, size_t* size) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && size != NULL)
    {
        b = smpool_size(h->node_pool, size);
    }
    return b;
}

bool slist_capacity(slist_t* l, size_t* capacity) {
    bool b = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && capacity != NULL)
    {
        b = smpool_capacity(h->node_pool, capacity);
    }
    return b;
}

bool slist_find(slist_t* l, slist_comparator_t cmp, void* right, void** e) {
    bool b = false;
    bool found = false;
    slist_header_t* h = (slist_header_t*)l;
    if (l != NULL
        && h->root_node != NULL
        && cmp != NULL
        && e != NULL)
    {
        // traverse from root to head
        for (node_t* head_node = h->root_node; 
             found == false 
             && head_node->head != NULL; 
             head_node = head_node->head)
        {
            found = cmp(head_node->elem, right);
            if (found == true)
            {
                *e = head_node->elem;
            }
        }

        // traverse from root to tail
        for (node_t* tail_node = h->root_node;
             found == false 
             && tail_node->tail != NULL;
             tail_node = tail_node->tail)
        {
            found = cmp(tail_node->elem, right);
            if (found == true)
            {
                *e = tail_node->elem;
            }
        }

        b = found;
    }
    return b;
}