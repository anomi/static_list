#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch2/catch.hpp"
#include "static_list/static_list.h"

TEST_CASE("static memory pool reserve macro", "[static_stack, smpool_reserve]") {
    /**
     * Unfortunately this are more alibi tests because C does not proved better options
     */
    SECTION( "static memory pool reserve normal" ) { 
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with 0 capacity") {
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with negative capacity") {
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with 0 element size") {
        REQUIRE(true == true);
    }
    SECTION("static memory pool reserve with negativ element size") {
        REQUIRE(true == true);
    }
}

TEST_CASE("static memory pool init function tests", "[static_memory_pool, smpool_init]") {
    const size_t elem_size = sizeof(int);
    const size_t capacity = 10;
    smpool_t smp[smpool_reserve(capacity, elem_size)];
    REQUIRE(NULL != memset(smp, 0, sizeof(smp)));

    SECTION("static memory pool init normal") {
        REQUIRE(true == smpool_init(smp, capacity, elem_size));
    }
    SECTION("static memory pool init with nullptr") {
        REQUIRE(false == smpool_init(NULL, capacity, elem_size));
    }
    SECTION("static memory pool init with 0 capacity") {
        REQUIRE(false == smpool_init(smp, 0, elem_size));
    }
    SECTION("static memory pool init with 0 element size") {
        REQUIRE(false == smpool_init(smp, capacity, 0));
    }
}
